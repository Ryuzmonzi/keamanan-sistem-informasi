from django.db import models

# Create your models here.

class User (models.Model):
	firstname = models.CharField(max_length=225)
	lastname = models.CharField(max_length=225)
	email = models.EmailField(max_length=225,unique=True)

	def ___str___(self):
		return "{}".format(self.email)
		