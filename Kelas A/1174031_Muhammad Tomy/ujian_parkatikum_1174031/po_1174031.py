import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ujian_parkatikum_1174031.settings')
import django
django.setup()
import random
from ujian_aplikasi_1174031.models import User
from faker import Faker

fakegen = Faker()
ndepan = ['fahmi','dzihan','habib','niko','rifky']

def populate(N=30):
    for entry in range(0,N):
        fakelast = fakegen.last_name()
        fakemail = fakegen.email()

        nakhir = User.objects.get_or_create(firstname=random.choice(ndepan),lasttname=fakelast,useremail=fakemail)[0]

if __name__ == '__main__':
    inputan = int(input("Masukan Angka = "))
    print("Tunggu Beberapa Saat ...")
    populate(inputan)
    print("data berhasil diisi")