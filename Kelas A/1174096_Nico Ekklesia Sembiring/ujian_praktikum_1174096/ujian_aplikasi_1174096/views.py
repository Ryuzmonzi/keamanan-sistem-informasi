from django.shortcuts import render
from django.http import HttpResponse
from ujian_aplikasi_1174096.models import User
# Create your views here.

def index(request):
    return render(request, 'ujian_aplikasi_1174096/index_1174096.html')

def users(request):
    sdata = User.objects.all()
    data = {
        'hasil': sdata,
    }
    return render(request, 'ujian_aplikasi_1174096/users_1174096.html', context=data)
