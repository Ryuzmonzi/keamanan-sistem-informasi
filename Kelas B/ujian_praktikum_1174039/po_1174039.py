import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE','ujian_praktikum_1174039.settings')

import django

django.setup()

import random
from ujian_aplikasi_1174039.models import User
from faker import Faker

fakegen = Faker()
users = ['Dika','Alit','Rangga','Teddy','Iqbal penggabean','Afra','Hagan','Lutfi','Kevin','Irvan','Najib','ichsan']

def populate(N=5):
	for entry in range (N):
		fake_lastname = fakegen.last_name()
		fake_email = fakegen.email()

		user= User.objects.get_or_create(firstname=random.choice(users),lastname=fake_lastname,email=fake_email)

if __name__=='__main__':
	print("pupolating")
	populate(90)
	print("selesai")

