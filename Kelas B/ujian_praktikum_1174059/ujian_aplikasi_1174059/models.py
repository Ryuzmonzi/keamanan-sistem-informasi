from django.db import models

# Create your models here.


class user(models.Model):
    first_name = models.CharField(max_length=265, unique=True)
    last_name = models.CharField(max_length=265, unique=True)
    email = models.EmailField(max_length=70, unique=True)

    class meta:
        db_table = "user"

    def __str__(self):
        return self.email
