from django.shortcuts import render
from django.http import HttpResponse
from ujian_aplikasi_1174050.models import User
# Create your views here.
def index(request):
    return render(request, 'ujian_aplikasi_1174050/index_1174050.html')

def users_1174050(request):
    users = User.objects.all()
    userdict = {'users': users}
    return render(request,'ujian_aplikasi_1174050/users_1174050.html',context= userdict)
    