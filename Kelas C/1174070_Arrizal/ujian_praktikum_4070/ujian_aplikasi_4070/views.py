from django.shortcuts import render
from django.http import HttpResponse
from .models import User
# Create your views here.
def index(request):
    return render(request,'ujian_aplikasi_1174070/index_1174070.html')

def users(request):
    user_profiles = User.objects.all()
    context= {'allvideos': user_profiles}
    return render (request,'ujian_aplikasi_1174070/users_1174070.html',context)