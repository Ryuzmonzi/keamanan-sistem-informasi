from django.shortcuts import render
from .models import User
# Create your views here.
def index(request):
     return render(request, 'ujian_aplikasi_1174080/index_1174080.html')

def users(request):
     sdata = User.objects.all()
     data = {
         'hasil': sdata,
     }
     return render(request, 'ujian_aplikasi_1174080/users_1174080.html', context=data)