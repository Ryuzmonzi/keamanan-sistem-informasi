import os
# Configure settings for project
# Need to run this before calling models from applications!
os.environ.setdefault('DJANGO_SETTINGS_MODULE','ujian_praktikum_1174066.settings')

import django
# Import settings
django.setup()


# Fake Script
import random
from ujian_aplikasi_1174066.models import User
from faker import Faker

fakegen = Faker()

def po_1174066(N=30):
    for entry in range (N):

        # Buat data menggunakan Faker
        fake_first_name = fakegen.first_name()
        fake_last_name = fakegen.last_name_male()
        fake_email = fakegen.email()

        # Memasukkan data ke User
        user = User.objects.get_or_create(first_name=fake_first_name, last_name=fake_last_name, email=fake_email)[0]

if __name__=='__main__':
    print("Populating the database..........Please Wait!")
    po_1174066(30)
    print("Populating Complete!")
