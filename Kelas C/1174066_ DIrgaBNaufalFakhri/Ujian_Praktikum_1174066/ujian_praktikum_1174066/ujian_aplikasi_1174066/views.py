from django.shortcuts import render
from .models import User

# Create your views here.
def index(request):
    return render(request,'ujian_aplikasi_1174066/index.html_1174066')

def userViews(request):
    user = User.objects.all()
    return render(request, 'ujian_aplikasi_1174066/users.html_1174066', {'user': user})

